import * as smartexpress from '@pushrocks/smartexpress';
import * as interfaces from '../ts/interfaces';

import { UserDataStore } from './mockserverstore';

class MockServer {
  userDataStore = new UserDataStore;
  expressInstance: smartexpress.Server;

  /**
   * starts the mock server
   */
  async start() {
    this.expressInstance = new smartexpress.Server({
      cors: true,
      defaultAnswer: async () => 'mockServer for userData',
      forceSsl: false,
      port: 3000
    });

    // SYNC
    this.expressInstance.addRoute('/synchronize', new smartexpress.Handler('POST', async (req, res) => {
      console.log(`Got a 'POST' request`);
      const requestBodyPOST: interfaces.IUserdataPart = JSON.parse(req.body);
      res.send(JSON.stringify(this.userDataStore.getUserdataPart(requestBodyPOST.identifier, true)));
      res.end();
    }));

    // CHANGES
    this.expressInstance.addRoute('/changes', new smartexpress.Handler('POST', async (req, res) => {

    }))

    // INITIAL
    this.expressInstance.addRoute('/initial', new smartexpress.Handler('POST', async (req, res) => {
      console.log('got request for /initial')
      const identifier: interfaces.IUserDataIdentifier = req.body;
      const userPart = this.userDataStore.getUserdataPart(identifier, true); // second parameter triggers autoAdd
      console.log(userPart);
      res.send(JSON.stringify(userPart));
      res.end();
    }));

    await this.expressInstance.start();
  }

  /**
   * stops the mockserver
   */
  async stop() {
    await this.expressInstance.stop();
  }

  constructor() { }
}

/**
 * the mock server instance
 * should be started and stopped using its async start() stop() functions
 */
export const mockServer = new MockServer();
