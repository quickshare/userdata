import { expect, tap } from '@pushrocks/tapbundle';

import { mockServer } from './mockserver';

import * as fetch from 'node-fetch';
// env preparation
declare global {
  namespace NodeJS {
    interface Global {
        fetch: any;
    }
  }
}
global.fetch = fetch;

import * as userdata from '../ts/index'

let testUserdataInstance: userdata.UserdataService;

const userDataPart1: userdata.IUserdataPart = {
  identifier: {
    applicationName: 'App1',
    userName: 'user1'
  },
  publicPayload: {},
  timestamp: Date.now()
};
const userDataPart2: userdata.IUserdataPart = {
  identifier: {
    applicationName: 'App2',
    userName: 'user2',
  },
  privatePayload: {},
  timestamp: Date.now()
};

const testIdentifier: userdata.IUserDataIdentifier = {
  applicationName: 'hello-world',
  userName: 'max@example.com'
};


tap.test('should start mockserver', async (tools) => {
  await mockServer.start();
  await tools.delayFor(2000);
});

tap.test('should create a valid instance of the UserDataService', async (tools) => {
  testUserdataInstance = new userdata.UserdataService({
    userdataServiceEndpoint: 'http://localhost:3000'
  });
});

tap.test('should accept an announcement of an userpart', async (tools) => {
  const communicator = await testUserdataInstance.announceUserDataIdentifier(testIdentifier);
  await communicator.sendChange(userDataPart1);
});

tap.test('should present a valid communicator', async (tools) => {
  const communicator = await testUserdataInstance.announceUserDataIdentifier(userDataPart1.identifier);
  expect(communicator).to.be.instanceOf(userdata.UserdataCommunicator);
});


tap.test('should get initial userdata', async (tools) => {
  await testUserdataInstance.sync();
  expect(testUserdataInstance.communicatorStore.length).to.be.greaterThan(0);
  expect(testUserdataInstance.communicatorStore[0].privatePayload).to.equal(userDataPart1.privatePayload);
});

tap.test('should attempt storage of new userdata', async (tools) => {
  await testUserdataInstance.communicatorStore[0].sendChange({});
});

tap.test('should stop mockserver', async (tools) => {
  await mockServer.stop();
});



tap.start();
