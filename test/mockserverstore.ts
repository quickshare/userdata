import * as interfaces from '../ts/interfaces';
import { IUserdataPart } from '../ts/interfaces';
import * as jsonStableStringify from 'json-stable-stringify';

export class UserDataStore {
  userdataParts: interfaces.IUserdataPart[] = [];

  /**
   * adds a userdataPart
   * @param userdataPart the userdataPart to add
   * @param forceAdd determines wether the userdataPart will be forceadded (potentially overwriting existing userdata)
   */
  addUserdataPart (userdataPart: interfaces.IUserdataPart, forceAdd: boolean = false) {
    const identifier = userdataPart.identifier;
    const existingUserDate = this.getUserdataPart(userdataPart.identifier);
    if (existingUserDate) {
      if(forceAdd) {
        this.removeUserdataPart(userdataPart.identifier)
      } else {
        console.log('userdata already exists');
        return
      }
    }


    this.userdataParts.push(userdataPart);
  }

  /**
   * merges a userdataPart
   * @param userdataPart
   */
  mergeUserdataPart (userdataPart: interfaces.IUserdataPart) {
    const identifier = userdataPart.identifier;
    const existingUserDate = this.getUserdataPart(identifier);

    if (existingUserDate) {
      // compare last change
      existingUserDate.timestamp
    } else {
      this.addUserdataPart(userdataPart);
    }
  };

  /**
   * gets a userdataPart by unique identifier
   * @param userdataPartIdentifierArg
   */
  getUserdataPart (userdataPartIdentifierArg: interfaces.IUserDataIdentifier, autoAdd: boolean = false): IUserdataPart {
    const wantedIdentifierStringified = jsonStableStringify(userdataPartIdentifierArg)
    let existingUserDataPart = this.userdataParts.find(userdataPartArg => {
      const comparisonStringified = jsonStableStringify(userdataPartArg.identifier);
      return wantedIdentifierStringified === comparisonStringified;
    });

    if(!existingUserDataPart && autoAdd) {
      console.log('userdata not yet existent')
      this.addUserdataPart({
        identifier: userdataPartIdentifierArg,
        timestamp: Date.now()
      })
      existingUserDataPart = this.getUserdataPart(userdataPartIdentifierArg);
    }

    console.log('got userdata!')
    return existingUserDataPart;
  }

  /**
   * removes user data from the user data store
   * @param userdataPartIdentifier
   */
  removeUserdataPart (userdataPartIdentifier: interfaces.IUserDataIdentifier) {
    const wantedIdentifierStringified = jsonStableStringify(userdataPartIdentifier)
    const newArray = this.userdataParts.filter(userdataPartArg => {
      const comparisonStringified = jsonStableStringify(userdataPartArg);
      return wantedIdentifierStringified !== comparisonStringified;
    });
  }
};