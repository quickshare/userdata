import * as plugins from './userdata.plugins';
import * as interfaces from './interfaces'

import { UserdataCommunicator } from './userdata.classes.communicator';

export interface IUserDataServiceOptions {
  userdataServiceEndpoint: string;
}

/**
 * the service that stores userdata
 */
export class UserdataService {
  options: IUserDataServiceOptions;
  probingActive = false;


  /**
   * the state of the userdata
   */
  state: interfaces.TUserDataState = 'warmingup'
  communicatorStore: UserdataCommunicator[] = [];

  /**
   * the main constructor for the userdata class
   */
  constructor(optionsArg: IUserDataServiceOptions) {
    this.options = optionsArg;
  }

  /**
   * starts the probing for changes
   * currently implemented using polling, may later on use websockets
   */
  startChangeProbing () {
    this.probingActive = true;
    this.probeForChanges();
  }

  /**
   * syncs the current settings from the client
   */
  sync () {
    for (const communicator of this.communicatorStore) {
      communicator.synchronize();
    }
  }

  /**
   * allows retrieval of historic user states
   * @param number
   */
  history(number: string) {
    // TODO
  }

  /**
   * sets up automatic saving before a page closes
   */
  syncBeforeExit() {
    window.addEventListener("unload", async (event) => {
      await this.sync();
    });
  }

  /**
   * announce a part id to userdata
   * this is to be used  by modules/components to add their userdata to the userdataservice
   * @returns observable
   */
  public async announceUserDataIdentifier (identifierArg: interfaces.IUserDataIdentifier) {
    const communicator = new UserdataCommunicator(this, identifierArg);
    this.communicatorStore.push(communicator);
    await communicator.init();
    return communicator;
  }

  /**
   * probes for changes
   */
  async probeForChanges () {
    while (this.probingActive) {
      await plugins.smartdelay.delayFor(5000);
    }
  }
}



