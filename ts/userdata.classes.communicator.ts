import * as plugins from './userdata.plugins';
import * as interfaces from './interfaces';

import { Observable } from 'rxjs';
import { UserdataService } from './userdata.classes.userdataservice';

/**
 * the user communicator is responsible for communicating with userdata clients
 */
export class UserdataCommunicator {
  // STATIC

  // INSTANCE
  userdataServiceRef: UserdataService;
  userdataIdentifier: interfaces.IUserDataIdentifier;
  
  // Payloads
  /**
   * a private payload is specific to the user and may be changed on a common basis
   */
  privatePayload: any;

  /**
   * a public payload is usually not to be changed by an individual user
   */
  publicPayload: any;

  timestamp: number; 

  /**
   * a change observable that allows reacting to a change that occured somewhere else
   */
  public changeObservable: Observable<interfaces.IUserdataPart>;

  constructor(userdataServiceRefArg: UserdataService, userdataIdentifierArg: interfaces.IUserDataIdentifier) {
    this.userdataServiceRef = userdataServiceRefArg;
    this.userdataIdentifier = userdataIdentifierArg;
  }
  
  /**
   * sends an active change resulting from direct user input
   * the local state will only be updated after confirmation through the server
   */
  async sendChange(userdataPayloadArg: any) {
    this.synchronize({
      identifier: this.userdataIdentifier,
      timestamp: Date.now(),
      privatePayload: userdataPayloadArg
    });
  }

  /**
   * syncs the change with the server
   */
  public async synchronize (userdataPartArg?: interfaces.IUserdataPart) {
    // determine which userdatapart to use
    const userdataForPayload: interfaces.IUserdataPart = userdataPartArg || {
      identifier: this.userdataIdentifier,
      timestamp: this.timestamp
    }
  

    // send current settings to backend
    // get response with merged userdataPartSettings
    const webrequestInstance = new plugins.webrequest.WebRequest();
    const requestUrl = this.userdataServiceRef.options.userdataServiceEndpoint + '/synchronize';
    const response = await webrequestInstance.request([requestUrl], {
      method: 'POST',
      body: JSON.stringify(userdataForPayload)
    });
    this.privatePayload = response.privatePayload
  }

  /**
   * Inits the communicator and performs an initial sync task
   */
  public async init () {
    const webrequestInstance = new plugins.webrequest.WebRequest();
    const requestUrl = this.userdataServiceRef.options.userdataServiceEndpoint + '/initial';
    console.log(requestUrl);
    const response = await webrequestInstance.request([requestUrl], {
      method: 'POST',
      body: JSON.stringify(this.userdataIdentifier)
    });
    console.log(response);
  }

};