import { IUserDataIdentifier } from "./userdataidentifier";

/**
 * a more or less independent userdata part of a larger application
 */
export interface IUserdataPart {
  /**
   * a part id that identifies the origin of the UserdataPart
   */
  identifier: IUserDataIdentifier;

  /**
   * a timestamp in milliseconds that shows the time of the last update
   */
  timestamp: number;

  /**
   * the public subpart
   */
  publicPayload?: any;

  /**
   * the private subpart of the userdata
   */
  privatePayload?: any;
};
