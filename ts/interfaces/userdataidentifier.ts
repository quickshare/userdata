/**
 * identifies a user data
 */
export interface IUserDataIdentifier {
  applicationName: string;
  userName: string;
}
