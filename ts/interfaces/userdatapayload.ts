import { IUserDataIdentifier, IUserdataPart } from ".";

/**
 * describes the body JSON payload of request between userdata client and server
 */
export interface IUserdataPayload {
  identifier: IUserDataIdentifier,
  userdataParts: IUserdataPart[]
}