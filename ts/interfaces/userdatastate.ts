/**
 * describes a the userdatastate
 */
export type TUserDataState = 'warmingup' | 'new' | 'initiallysynced' | 'synced' | 'finallysaved';

