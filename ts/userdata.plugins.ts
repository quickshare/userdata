import * as smartpromise from '@pushrocks/smartpromise';
import * as smartdelay from '@pushrocks/smartdelay';
import * as webrequest from '@pushrocks/webrequest';

// rxjs is pretty big
// this is why we make sure we only import the essentials so that treeshaking works during bundling
import { Observable } from 'rxjs';

export const rxjs = {
  Observable
}

export {
  smartpromise,
  smartdelay,
  webrequest
}
